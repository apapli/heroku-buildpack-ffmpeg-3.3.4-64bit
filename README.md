# heroku-buildpack-ffmpeg-latest


A Heroku buildpack for ffmpeg that always downloads the latest [static build](http://johnvansickle.com/ffmpeg/).
Unlike other build packs, I never compile anything.


## Credits

All credits for this buildpack should go to Giddyio, which this repo has been cloned from:

https://github.com/giddyio/heroku-buildpack-ffmpeg-latest

